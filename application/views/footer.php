<br> <br><br>
<footer class="tm-container-outer" style="padding: 20px;">
    <p class="mb-0">Desarrollado por JDA © <span class="tm-current-year">2023</span> Universidad Técnica de Cotopaxi

    . Developed by students of Information Systems <a rel="nofollow" href="https://www.facebook.com/gadlamana/" target="_parent">FACEBOOK <i class="fa fa-facebook" aria-hidden="true"></i></a></p>
</footer>
</div>
</div> <!-- .main-content -->

<!-- load JS files -->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>             <!-- jQuery (https://jquery.com/download/) -->
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>                    <!-- https://popper.js.org/ -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>                 <!-- https://getbootstrap.com/ -->
<script src="<?php echo base_url(); ?>assets/js/datepicker.min.js"></script>                <!-- https://github.com/qodesmith/datepicker -->
<script src="<?php echo base_url(); ?>assets/js/jquery.singlePageNav.min.js"></script>      <!-- Single Page Nav (https://github.com/ChrisWojcik/single-page-nav) -->
<script src="<?php echo base_url(); ?>assets/slick/slick.min.js"></script>                  <!-- http://kenwheeler.github.io/slick/ -->
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>           <!-- https://github.com/flesler/jquery.scrollTo -->
<script>
/* Google Maps
------------------------------------------------*/
var map = '';
var center;

function initialize() {
var mapOptions = {
    zoom: 16,
    center: new google.maps.LatLng(37.769725, -122.462154),
    scrollwheel: false
};

map = new google.maps.Map(document.getElementById('google-map'),  mapOptions);

google.maps.event.addDomListener(map, 'idle', function() {
  calculateCenter();
});

google.maps.event.addDomListener(window, 'resize', function() {
  map.setCenter(center);
});
}

function calculateCenter() {
center = map.getCenter();
}

function loadGoogleMap(){
var script = document.createElement('script');
script.type = '<?php echo base_url(); ?>assets/text/javascript';
script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDVWt4rJfibfsEDvcuaChUaZRS5NXey1Cs&v=3.exp&sensor=false&' + 'callback=initialize';
document.body.appendChild(script);
}

/* DOM is ready
------------------------------------------------*/
$(function(){

// Change top navbar on scroll
$(window).on("scroll", function() {
    if($(window).scrollTop() > 100) {
        $(".tm-top-bar").addClass("active");
    } else {
     $(".tm-top-bar").removeClass("active");
    }
});

// <?php if (!$this->session->userdata("conectado")): ?>
//   $('.tm-down-arrow-link').click(function(){
//       $.scrollTo('#tm-section-search', 300, {easing:'linear'});
//   });
// <?php else: ?>
//   // Smooth scroll to search form
//   $('.tm-down-arrow-link').click(function(){
//       $.scrollTo('#ya_iniciado', 300, {easing:'linear'});
//   });
// <?php endif; ?>


// Date Picker in Search form
var pickerCheckIn = datepicker('#inputCheckIn');
var pickerCheckOut = datepicker('#inputCheckOut');

// Update nav links on scroll
$('#tm-top-bar').singlePageNav({
    currentClass:'active',
    offset: 60
});

// Close navbar after clicked
$('.nav-link').click(function(){
    $('#mainNav').removeClass('show');
});

// Slick Carousel
$('.tm-slideshow').slick({
    infinite: true,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1
});

loadGoogleMap();                                       // Google Map
$('.tm-current-year').text(new Date().getFullYear());  // Update year in copyright
});

</script>

<?php if (!$this->session->userdata("conectad0")): ?>
<script>
  $('.tm-down-arrow-link').click(function(){
      $.scrollTo('#tm-section-search', 300, {easing:'linear'});
  });
</script>
<?php else: ?>
<script>
  $('.tm-down-arrow-link').click(function(){
      $.scrollTo('#ya_iniciado', 300, {easing:'linear'});
  });
</script>
<?php endif; ?>



<style>

      label.error{
          border:1px solid white;
          color:white;
          background-color:#E15B69;
          padding:5px;
          padding-left:15px;
          padding-right:15px;
          font-size:12px;
          opacity: 0;
            /visibility: hidden;/
            /position: absolute;/
            left: 0px;
            transform: translate(0, 10px);
            /background-color: white;/
            /padding: 1.5rem;/
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
            width: auto;
            margin-top:30px !important;


            z-index: 10;
            opacity: 1;
            visibility: visible;
            transform: translate(0, -20px);
            transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
            border-radius:10px;
            width:100%;

      }

      input.error{
          border:1px solid #E15B69;
      }


      select.error{
          border:1px solid #E15B69;
      }

      label.error:before{
           position: absolute;
            z-index: -1;
            content: "";
            right: calc(90% - 10px);
            top: -8px;
            border-style: solid;
            border-width: 0 10px 10px 10px;
            border-color: transparent transparent #E15B69 transparent;
            transition-duration: 0.3s;
            transition-property: transform;
      }


  </style>
  <?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
    iziToast.success({
         title: 'CONFIRMACIÓN',
         message: '<?php echo $this->session->flashdata("confirmacion"); ?>',
         position: 'topRight',
       });
  </script>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
    iziToast.danger({
         title: 'ADVERTENCIA',
         message: '<?php echo $this->session->flashdata("error"); ?>',
         position: 'topRight',
       });
  </script>
<?php endif; ?>


<?php if ($this->session->flashdata("bienvenida")): ?>
  <script type="text/javascript">
    iziToast.info({
         title: 'CONFIRMACIÓN',
         message: '<?php echo $this->session->flashdata("bienvenida"); ?>',
         position: 'topRight',
       });
  </script>
<?php endif; ?>


<style media="screen">
    .error{
      color:red;
      font-size: 16px;
    }
    input.error, select.error{
      border: 2px solid red;
    }
</style>


</body>
</html>
