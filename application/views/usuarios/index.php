<div class="tm-container-outer tm-bg-primary" style="padding: 7px; color: white;">
  <div class="row">
    <div class="col-md-8" style="">
      <h3 class=""> <b>LISTADO DE USUARIOS  </b> </h3
       >
    </div>
    <div class="col-md-4 text-right">
      <a href="#" class="text-uppercase tm-btn tm-btn-white tm-btn-white-primary" data-toggle="modal" data-target="#myModal">AGREGAR USUARIO</a>
    </div>

  </div>
</div>
<div class="tm-container-outer" style="padding: 15px; color: white;">
  <div class="row">
    <div class="col-md-12" style="background:white; color: black;">
      <br>
      <?php if ($listadoUsuarios): ?>
        <table id="tbl-usuario" class="table table-striped table-bordered table-hover" >
          <thead>
            <tr>
              <th class="text-center">ID</th>
              <th class="text-center">NOMBRE</th>
              <th class="text-center">PRIMER APELLIDO</th>
              <th class="text-center">SEGUNDO APELLIDO</th>
              <th class="text-center">EMAIL</th>
              <th class="text-center">ROL</th>
              <th class="text-center">ACCIONES</th>

            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoUsuarios->result() as $usuarioTemporal): ?>
              <tr>
                <td class="text-center"><?php echo $usuarioTemporal->id_usu; ?></td>
                <td class="text-center"><?php echo $usuarioTemporal->nombre_usu; ?></td>
                <td class="text-center"><?php echo $usuarioTemporal->primer_apellido_usu; ?></td>
                <td class="text-center"><?php echo $usuarioTemporal->segundo_apellido_usu; ?></td>
                <td class="text-center"><?php echo $usuarioTemporal->email_usu; ?></td>
                <td class="text-center"><?php echo $usuarioTemporal->rol_usu; ?></td>
                <td class="text-center">
                  <a href="<?php echo site_url('usuarios/actualizar'); ?>/<?php echo $usuarioTemporal->id_usu; ?>" class=""> <i class="fa fa-pencil yellow-icon" ></i></a>
                  <a href="<?php echo site_url('usuarios/borrar'); ?>/<?php echo $usuarioTemporal->id_usu; ?>"class=""> <i class="fa fa-trash red-icon"></i></a>
                </td>
                 </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <script type="text/javascript">
          $("#tbl-usuario").DataTable({
            language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json'
                  }
            });

        </script>
        <br>

      <?php else: ?>
        <h3><b>RUTAS NO REGISTRADAS</b></h3>
      <?php endif; ?>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#tbl-usuario").DataTable();
</script>

<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <br><br><br><br><br>
    <div class="modal-content">

      <!-- Encabezado del modal -->
      <div class="modal-header">
        <h5 class="modal-title">REGISTRO DE USUARIO</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Contenido del modal -->
      <div class="modal-body">
        <form class="" id="frm_nuevo_tipo" class="" enctype="multipart/form-data" action="<?php echo site_url('usuarios/guardarUsuarios'); ?>" method="post">
          <div class="row">
            <div class="col-md-4">
                  <label for="">NOMBRE </label>
                  <input type="text" id="nombre_usu" name="nombre_usu" value="" class="form-control" placeholder="Ingrese el nombre del usuario" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">PRIMER APELLIDO</label>
                  <input type="text" id="primer_apellido_usu" name="primer_apellido_usu" value="" class="form-control" placeholder="Ingrese el primer apellido" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">SEGUNDO APELLIDO</label>
                  <input type="text" id="segundo_apellido_usu" name="segundo_apellido_usu" value="" class="form-control" placeholder="Ingrese el segundo apellido" required>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
                  <label for="">EMAIL</label>
                  <input type="text" id="email_usu" name="email_usu" value="" class="form-control" placeholder="Ingrese el email" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">ROL</label>
                  <select class="form-control" name="rol_usu" id="rol_usu" required data-live-search="true">
                <!-- cargar opciones de <select class="" name="">  </select> -->
                <option value="">Seleccione un Rol</option>
                <option value="Administrador">Administrador</option>
                <option value="Empleado">Empleado</option>

              </select>
              <br>


          </div>
          </div>
          <div class="row">

          <div class="col-md-4">
            <label for="password" class="form-label">Contraseña:</label>
            <input type="password" class="form-control" id="password_usu" name="password_usu" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$" placeholder="Ingrese una contraseña" required>
            <input type="checkbox" id="showPassword">
            <label for="showPassword">Mostrar contraseña</label>
            <br>
          </div>
          <div class="col-md-4">
            <label for="password_repeat" class="form-label">Repetir Contraseña:</label>
            <input type="password" class="form-control" id="password_repeat" name="password_repeat" placeholder="Repita la contraseña" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$" placeholder="Repita la contraseña">
            <input type="checkbox" id="showPassword_repeat">
            <label for="showPassword_repeat">Mostrar contraseña</label>
            <div class="form-text">La contraseña debe tener al menos 8 caracteres, incluyendo al menos una letra mayúscula, una letra minúscula, un número y un carácter especial.</div>
          </div>

          </div>


          </div>

        <div class="modal-footer">
          <a href="<?php echo site_url('rutas/index'); ?>" class="btn btn-danger">
            <i class="glyphicon glyphicon-remove"></i>
            Cancelar
          </a>
          <button type="submit" name="button" class="btn btn-primary">
            <i class="glyphicon glyphicon-ok"></i>
            Guardar
          </button>
        </div>
        </form>
      </div>

      <!-- Pie del modal -->


    </div>
  </div>
</div>
<script>
  const passwordInput = document.getElementById('password_usu');
  const showPasswordCheckbox = document.getElementById('showPassword');
  const passwordRepeatInput = document.getElementById('password_repeat');
  const showPasswordRepeatCheckbox = document.getElementById('showPassword_repeat');

  showPasswordCheckbox.addEventListener('change', function() {
    if (showPasswordCheckbox.checked) {
      passwordInput.type = 'text';
    } else {
      passwordInput.type = 'password';
    }
  });

  showPasswordRepeatCheckbox.addEventListener('change', function() {
    if (showPasswordRepeatCheckbox.checked) {
      passwordRepeatInput.type = 'text';
    } else {
      passwordRepeatInput.type = 'password';
    }
  });


    document.getElementById("frm_nuevo_tipo").addEventListener("submit", function(event) {
      var password = document.getElementById("password_usu").value;
      var passwordRepeat = document.getElementById("password_repeat").value;

      if (password !== passwordRepeat) {
        alert("Las contraseñas no coinciden. Por favor, inténtelo de nuevo.");
        event.preventDefault(); // Evita que el formulario se envíe
      }
    });
</script>
