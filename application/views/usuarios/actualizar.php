<div class="tm-container-outer tm-bg-primary" style="padding: 7px; color: white;">
  <div class="row">
    <div class="col-md-8" style="">
      <h2 class=""> <b>ACTUALIZAR USUARIOS  </b> </h2>
    </div>
<br>

  </div>
</div><br>
<div class="row " >
  <div class="col-md-4">

  </div>
  <div class="col-md-4 bg-white">
    <form class="" id="frm_nuevo_tipo" class="" enctype="multipart/form-data" action="<?php echo site_url('usuarios/procesoActualizacion'); ?>" method="post">
      <div class="row">
        <input type="hidden" name="id_usu" value="<?php echo $usuariosEditar->id_usu; ?>">
        <div class="col-md-12">
              <label for="">NOMBRE </label>
              <input type="text" id="nombre_usu" name="nombre_usu" value="<?php echo $usuariosEditar->nombre_usu ?>" class="form-control" placeholder="Ingrese el nombre del usuario" required>
          </div>
          <br>
          <div class="col-md-12">
                <label for="">PRIMER APELLIDO</label>
                <input type="text" id="primer_apellido_usu" name="primer_apellido_usu" value="<?php echo $usuariosEditar->primer_apellido_usu ?>" class="form-control" placeholder="Ingrese el primer apellido" required>
            <br>
          </div>
          <br>
          <div class="col-md-12">
                <label for="">SEGUNDO APELLIDO</label>
                <input type="text" id="segundo_apellido_usu" name="segundo_apellido_usu" value="<?php echo $usuariosEditar->segundo_apellido_usu ?>" class="form-control" placeholder="Ingrese el segundo apellido" required>
            <br>
          </div>
          <div class="col-md-12">
                <label for="">EMAIL</label>
                <input type="text" id="email_usu" name="email_usu" value="<?php echo $usuariosEditar->email_usu ?>" class="form-control" placeholder="Ingrese el email" required>
            <br>
          </div>
          <div class="col-md-12">
                <label for="">ROL</label>
                <select class="form-control" name="rol_usu" id="rol_usu"  required data-live-search="true">
              <!-- cargar opciones de <select class="" name="">  </select> -->
              <option value="">Seleccione un Rol</option>
              <option value="Administrador">Administrador</option>
              <option value="Empleado">Empleado</option>

            </select>
            </div>
            <br>
            <div class="col-md-12">
              <label for="password_usu" class="form-label">Contraseña:</label>
              <input type="password" class="form-control" id="password_usu" name="password_usu" value="<?php echo $usuariosEditar->password_usu ?>" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$" placeholder="Ingrese una contraseña" required>
              <input type="checkbox" id="showPassword_usu">
              <label for="showPassword_usu">Mostrar contraseña</label>
              <br>
            </div>

            <div class="col-md-12">
              <label for="password_repeat" class="form-label">Repetir Contraseña:</label>
              <input type="password" class="form-control" id="password_repeat" name="password_repeat" placeholder="Repita la contraseña" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$" placeholder="Repita la contraseña">
              <input type="checkbox" id="showPassword_repeat">
              <label for="showPassword_repeat">Mostrar contraseña</label>
              <div class="form-text">La contraseña debe tener al menos 8 caracteres, incluyendo al menos una letra mayúscula, una letra minúscula, un número y un carácter especial.</div>
            </div>
        </div>




      <div class="modal-footer">
        <a href="<?php echo site_url('usuarios/index'); ?>" class="btn btn-danger">
          <i class="glyphicon glyphicon-remove"></i>
          Cancelar
        </a>
        <button type="submit" name="button" class="btn btn-primary">
          <i class="glyphicon glyphicon-ok"></i>
          Actualizar
        </button>
      </div>
      </form>
        </div>
      </div>
</div>
<div class="col-md-4">

</div>
</div>

<script type="text/javascript">
  $("#rol_usu").val("<?php echo $usuariosEditar->rol_usu ?>");
</script>
<script>
const passwordUsuInput = document.getElementById('password_usu');
const showPasswordUsuCheckbox = document.getElementById('showPassword_usu');
const passwordRepeatInput = document.getElementById('password_repeat');
const showPasswordRepeatCheckbox = document.getElementById('showPassword_repeat');

showPasswordUsuCheckbox.addEventListener('change', function() {
  if (showPasswordUsuCheckbox.checked) {
    passwordUsuInput.type = 'text';
  } else {
    passwordUsuInput.type = 'password';
  }
});

showPasswordRepeatCheckbox.addEventListener('change', function() {
  if (showPasswordRepeatCheckbox.checked) {
    passwordRepeatInput.type = 'text';
  } else {
    passwordRepeatInput.type = 'password';
  }
});



  document.getElementById("frm_nuevo_tipo").addEventListener("submit", function(event) {
    var password = document.getElementById("password_usu").value;
    var passwordRepeat = document.getElementById("password_repeat").value;

    if (password !== passwordRepeat) {
      alert("Las contraseñas no coinciden. Por favor, inténtelo de nuevo.");
      event.preventDefault(); // Evita que el formulario se envíe
    }
  });


</script>
