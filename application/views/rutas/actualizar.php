<div class="tm-container-outer tm-bg-primary" style="padding: 7px; color: white;">
  <div class="row">
    <div class="col-md-8" style="">
      <h2 class=""> <b> EDITAR RUTA </b> </h2>
    </div>
    <!-- <div class="col-md-4 text-right">
      <h2 class=""> <b> <?php echo $rutaEditar->lugar_ruta; ?> </b> </h2>
    </div> -->


  </div>
</div>

<div class="tm-container-outer" style="padding: 15px; color: white;">
  <div class="row">
    <div class="col-md-12" style="background:white; color: black;">
      <br>
      <?php if ($rutaEditar): ?>
        <form class="" id="frm_editar_ruta" class="" enctype="multipart/form-data" action="<?php echo site_url('rutas/procesarActualizacion'); ?>" method="post">
          <input type="hidden" name="id_ruta" value="<?php echo $rutaEditar->id_ruta; ?>">
          <div class="row">
            <div class="col-md-4">
                  <label for="">LUGAR:</label>
                  <input type="text" id="lugar_ruta" name="lugar_ruta" value="<?php echo $rutaEditar->lugar_ruta; ?>" class="form-control" placeholder="Ingrese el nombre del lugar" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">GRUPO:</label>
                  <input type="text" id="grupo_ruta" name="grupo_ruta" value="<?php echo $rutaEditar->grupo_ruta; ?>" class="form-control" placeholder="Ingrese el número del grupo" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">DURACIÓN:</label>
                  <input type="text" id="duracion_ruta" name="duracion_ruta" value="<?php echo $rutaEditar->duracion_ruta; ?>" class="form-control" placeholder="Ingrese la duración" required>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
                  <label for="">CÓDIGO:</label>
                  <input type="text" id="codigo_ruta" name="codigo_ruta" value="<?php echo $rutaEditar->codigo_ruta; ?>" class="form-control" placeholder="Ingrese el codigo de la ruta" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">DIFICULTAD:</label>
                  <input type="text" id="dificultad_ruta" name="dificultad_ruta" value="<?php echo $rutaEditar->dificultad_ruta; ?>" class="form-control" placeholder="Ingrese la dificultad" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">AUDIENCIA:</label>
                  <input type="text" id="audiencia_ruta" name="audiencia_ruta" value="<?php echo $rutaEditar->audiencia_ruta; ?>" class="form-control" placeholder="Ingrese el tipo de audiencia" required>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
                  <label for="">ATRACTIVOS:</label>
                  <textarea class="form-control"  name="atractivos_ruta" id="atractivos_ruta" rows="3" cols="" placeholder="Ingresa tu mensaje"><?php echo $rutaEditar->atractivos_ruta; ?></textarea>
                  <!-- <input type="text" id="atractivos_ruta" name="atractivos_ruta" value="<?php echo $rutaEditar->atractivos_ruta; ?>" class="form-control" placeholder="Ingrese los atractivos de la ruta" required> -->
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
              <a href="<?php echo site_url('rutas/index'); ?>" class="btn btn-danger"> <i class="glyphicon glyphicon-remove"></i>CANCELAR </a>
              <button type="submit" class="btn btn-primary">GUARDAR</button>
            </div>
          </div>
          <br><br>
        </div>

        </form>
      <?php endif; ?>
    </div>
  </div>
</div>
