<div class="tm-container-outer tm-bg-primary" style="padding: 7px; color: white;">
  <div class="row">
    <div class="col-md-8" style="">
      <h3 class=""> <b>LISTADO DE RUTAS  </b> </h3>
    </div>
    <div class="col-md-4 text-right">
      <a href="#" class="text-uppercase tm-btn tm-btn-white tm-btn-white-primary" data-toggle="modal" data-target="#myModal">AGREGAR RUTA</a>
    </div>

  </div>
</div>
<div class="tm-container-outer" style="padding: 15px; color: white;">
  <div class="row">
    <div class="col-md-12" style="background:white; color: black;">
      <br>
      <?php if ($listadoRutas): ?>
        <table id="tbl-rutas" class="table table-striped table-bordered table-hover" >
          <thead>
            <tr>
              <th class="text-center">ID</th>
              <th class="text-center">LUGAR</th>
              <th class="text-center">CÓDIGO</th>
              <th class="text-center">DURACIÓN</th>
              <th class="text-center">GRUPO</th>
              <th class="text-center">DIFICULTAD</th>
              <th class="text-center">AUDIENCIAS</th>
              <th class="text-center">ATRACTIVOS</th>
              <th class="text-center">ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoRutas->result() as $rutaTemporal): ?>
              <tr>
                <td class="text-center"><?php echo $rutaTemporal->id_ruta; ?></td>
                <td class="text-center"><?php echo $rutaTemporal->lugar_ruta; ?></td>
                <td class="text-center"><?php echo $rutaTemporal->codigo_ruta; ?></td>
                <td class="text-center"><?php echo $rutaTemporal->duracion_ruta; ?></td>
                <td class="text-center"><?php echo $rutaTemporal->grupo_ruta; ?></td>
                <td class="text-center"><?php echo $rutaTemporal->dificultad_ruta; ?></td>
                <td class="text-center"><?php echo $rutaTemporal->audiencia_ruta; ?></td>
                <td class="text-center"><?php echo $rutaTemporal->atractivos_ruta; ?></td>
                <td class="text-center">
                  <a href="<?php echo site_url("rutas/actualizar"); ?>/<?php echo $rutaTemporal->id_ruta; ?>" class="btn btn-warning"> <i class="fa fa-pencil"></i></a>
                  <a href="<?php echo site_url('rutas/borrar'); ?>/<?php echo $rutaTemporal->id_ruta; ?>"class="btn btn-danger"> <i class="fa fa-trash"></i></a>
                  <br><br>
                  <!-- <a href="<?php echo site_url('jugadores/registrar').'/'.$equipoTemporal->id_equ_ej; ?>"class="btn btn-primary"> <i class="fa fa-look"></i>Gestinar Equipo</a></td> -->
                  <a href="<?php echo site_url('lugares/index').'/'.$rutaTemporal->id_ruta; ?>"class="btn btn-primary">GESTIONAR</a>
             </tr>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else: ?>
        <h3><b>RUTAS NO REGISTRADAS</b></h3>
      <?php endif; ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#tbl-rutas").DataTable({
    language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json'
          }
    });

</script>
<!-- Modal para agregar rutas -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <br><br><br><br><br>
    <div class="modal-content">

      <!-- Encabezado del modal -->
      <div class="modal-header">
        <h5 class="modal-title">AGREGAR RUTA</h5>
        <button type="button" id="cancel" onclick="window.location.href='<?php echo site_url('rutas/index'); ?>'"class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Contenido del modal -->
      <div class="modal-body">
        <form class="" id="frm_nuevo_ruta" class="" enctype="multipart/form-data" action="<?php echo site_url('rutas/guardarRuta'); ?>" method="post">
          <div class="row">
            <div class="col-md-4">
                  <label for="">LUGAR:</label>
                  <input type="text" id="lugar_ruta" name="lugar_ruta" value="" class="form-control" placeholder="Ingrese el nombre del lugar">
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">GRUPO:</label>
                  <input type="text" id="grupo_ruta" name="grupo_ruta" value="" class="form-control" placeholder="Ingrese el número del grupo" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">DURACIÓN:</label>
                  <input type="text" id="duracion_ruta" name="duracion_ruta" value="" class="form-control" placeholder="Ingrese la duración" required>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
                  <label for="">CÓDIGO:</label>
                  <input type="text" id="codigo_ruta" name="codigo_ruta" value="" class="form-control" placeholder="Ingrese el codigo de la ruta" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">DIFICULTAD:</label>
                  <input type="text" id="dificultad_ruta" name="dificultad_ruta" value="" class="form-control" placeholder="Ingrese la dificultad" required>
              <br>
            </div>
            <div class="col-md-4">
                  <label for="">AUDIENCIA:</label>
                  <input type="text" id="audiencia_ruta" name="audiencia_ruta" value="" class="form-control" placeholder="Ingrese el tipo de audiencia" required>
              <br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
                  <label for="">ATRACTIVOS:</label>
                  <textarea class="form-control"  name="atractivos_ruta" id="atractivos_ruta" rows="3" cols="50" placeholder="Ingresa los atractivos que existan"></textarea>
                  <!-- <input type="text" id="atractivos_ruta" name="atractivos_ruta" value="" class="form-control" placeholder="Ingrese los atractivos de la ruta" required> -->
              <br>
            </div>
          </div>
        </div>
            <!-- Pie del modal -->
        <div class="modal-footer">
          <button type="button" id="cancel"class="btn btn-danger" onclick="window.location.href='<?php echo site_url('rutas/index'); ?>'" data-dismiss="modal">CANCELAR</button>
          <button type="submit" class="btn btn-primary">GUARDAR</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Script de validacion para el agregar -->
<script type="text/javascript">
  $("#frm_nuevo_ruta").validate({
    rules:{
      lugar_ruta:{
        required:true,
        minlength:4
      }
    },

    messages:{
      nombre_fas_ej:{
        required:"Ingrese el nombre de la ruta",
        minlength:"Nombre Incorrecto"
      }
    }
  });
</script>
