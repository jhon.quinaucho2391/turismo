<div class="tm-page-wrap mx-auto">
		<section class="tm-banner">
			 <div class="tm-container-outer tm-banner-bg"> 	<!--Imagen que toca cambiar pero no se como jajaj  -->
						<div class="container">

								<div class="row tm-banner-row tm-banner-row-header">
										<div class="col-xs-12">
												<div class="tm-banner-header">
														<h1 class="text-uppercase tm-banner-title">Vamos a empezar </h1>
														<img src="<?php echo base_url(); ?>assets/img/dots-3.png" alt="Dots">
														<p class="tm-banner-subtitle">Te ayudamos a elegir lo mejor.</p>
														<a href="javascript:void(0)" class="tm-down-arrow-link"><i class="fa fa-2x fa-angle-down tm-down-arrow"></i></a>
												</div>
										</div>  <!-- col-xs-12 -->
								</div> <!-- row -->
								<?php if (!$this->session->userdata("conectad0")): ?>
									<div class="row tm-banner-row" id="tm-section-search">


											<form class="tm-search-form tm-section-pad-2" id="frm_login" method="post"  action="<?php echo site_url('welcome/validarUsuario'); ?>">
													<div class="form-row tm-search-form-row">
															<div class="form-group tm-form-group tm-form-group-pad tm-form-group-1">
																<h5 class="text-center">
																	<label for="inputCity"> <b>Iniciar sesion</b> </label>
																</h5> <br>
																	<label for="inputCity">Usuario</label>
																	<input type="email" class="form-control" id="email_usu" name="email_usu" placeholder="Ingrese sus Usuario">
																	<label for="inputCity">Password</label>
																	<input type="password" class="form-control" id="password_usu" name="password_usu" placeholder="Ingrese su contraseña	">
															</div>
															<div class="form-group tm-form-group tm-form-group-pad tm-form-group-1 text-center"> <br><br><br><br>
																	<img src="https://png.pngtree.com/png-vector/20191003/ourlarge/pngtree-user-login-or-authenticate-icon-on-gray-background-flat-icon-ve-png-image_1786166.jpg" width="150" height="150"alt="Dots">
															</div>
															<div class="form-group tm-form-group tm-form-group-1">
																	<div class="form-group tm-form-group tm-form-group-pad tm-form-group-2">
																			<!-- <label for="inputRoom">How many rooms?</label> -->
																			<!-- <select name="room" class="form-control tm-select" id="inputRoom">
																					<option value="1" selected>1 Room</option>
																					<option value="2">2 Rooms</option>
																					<option value="3">3 Rooms</option>
																					<option value="4">4 Rooms</option>
																					<option value="5">5 Rooms</option>
																					<option value="6">6 Rooms</option>
																					<option value="7">7 Rooms</option>
																					<option value="8">8 Rooms</option>
																					<option value="9">9 Rooms</option>
																					<option value="10">10 Rooms</option>
																			</select> -->
																	</div>
																	<!-- <div class="form-group tm-form-group tm-form-group-pad tm-form-group-3">
																			<label for="inputAdult">Adult</label>
																			<select name="adult" class="form-control tm-select" id="inputAdult">
																					<option value="1" selected>1</option>
																					<option value="2">2</option>
																					<option value="3">3</option>
																					<option value="4">4</option>
																					<option value="5">5</option>
																					<option value="6">6</option>
																					<option value="7">7</option>
																					<option value="8">8</option>
																					<option value="9">9</option>
																					<option value="10">10</option>
																			</select>
																	</div> -->
																	<!-- <div class="form-group tm-form-group tm-form-group-pad tm-form-group-3">

																			<label for="inputChildren">Children</label>
																			<select name="children" class="form-control tm-select" id="inputChildren">
																				<option value="0" selected>0</option>
																					<option value="1">1</option>
																					<option value="2">2</option>
																					<option value="3">3</option>
																					<option value="4">4</option>
																					<option value="5">5</option>
																					<option value="6">6</option>
																					<option value="7">7</option>
																					<option value="8">8</option>
																					<option value="9">9</option>
																			</select>
																	</div> -->
															</div>
													</div> <!-- form-row -->
													<!-- <div class="form-row tm-search-form-row"> -->

															<!-- <div class="form-group tm-form-group tm-form-group-pad tm-form-group-3">
																	<label for="inputCheckIn">Check In Date</label>
																	<input name="check-in" type="text" class="form-control" id="inputCheckIn" placeholder="Check In">
															</div> -->
															<!-- <div class="form-group tm-form-group tm-form-group-pad tm-form-group-3">
																	<label for="inputCheckOut">Check Out Date</label>
																	<input name="check-out" type="text" class="form-control" id="inputCheckOut" placeholder="Check Out">
															</div> -->
															<div class="form-group tm-form-group tm-form-group-pad tm-form-group-1">
																	<!-- <label for="btnSubmit">&nbsp;</label> -->
																	<button type="submit" class="btn btn-primary tm-btn tm-btn-search text-uppercase" id="btnSubmit">INGRESAR</button>
															</div>
													<!-- </div> -->
											</form>

									</div> <!-- row -->
								<?php endif; ?>



								<div class="tm-banner-overlay"></div>
						</div>  <!-- .container -->
				</div>     <!-- .tm-container-outer -->
		</section>

		<section class="p-5 tm-container-outer tm-bg-gray" >
				<div class="container">
						<div class="row tm-banner-row" id="ya_iniciado">
								<div class="col-xs-12 mx-auto tm-about-text-wrap text-center">
										<h2 class="text-uppercase mb-4">TUS <strong>VIAJES </strong>ES NUESTRA PRIORIDAD
</h2>
										<p class="mb-4">Tu ruta hacia la aventura. Senderos de alta montaña o caminos de tierra solitarios. Sea cual sea tu destino, Turismo.com te lleva hasta él y te trae de vuelta </p>
										<!-- <a href="#" class="text-uppercase btn-primary tm-btn">Continue explore</a> -->
								</div>
						</div>
				</div>
		</section>

		<div class="tm-container-outer" id="tm-section-2">
				<section class="tm-slideshow-section">
						<div class="tm-slideshow">
								<img src="https://upload.wikimedia.org/wikipedia/commons/c/c5/Parque_Central_de_La_Man%C3%A1.jpg" alt="Image">

						</div>
						<div class="tm-slideshow-description tm-bg-primary">
									<h2 class="">Los lugares más visitados del Cantón la Mana</h2>
								<p>Chorrera de Zapanal: Las siete cascadas del Zapanal, un refugio de descanso donde sentirá la naturaleza y respirará aire puro.

                  Cueva de los Murciélagos: Es una grieta que se halla entre matorrales y ofrece al turista dos entradas; en este sitio habita gran cantidad de murciélagos.

                  Cascada del Oso: Un misterio o fantasía son las sensaciones que se perciben ante tan increíble fenómeno de la naturaleza, debido a su gran tamaño, algo asombroso.

                  Se recomienda también ir hacia la cascada El Mirador para obtener una vista magnífica que quede impresa en los recuerdos. Desde esta se pueden observar todas las llanuras y, además, disfrutar de su caída de agua de más de 60 metros.</p>
								<button  type="button" class="text-uppercase tm-btn tm-btn-white tm-btn-white-primary">Tierra de las Siete Cascadas</button>

						</div>
				</section>
				<section class="clearfix tm-slideshow-section tm-slideshow-section-reverse">

						<div class="tm-right tm-slideshow tm-slideshow-highlight">
								<img src="https://blog.carsync.com/hs-fs/hubfs/Imported_Blog_Media/optimizacion-de-rutas-2.png?width=730&height=223&name=optimizacion-de-rutas-2.png"width=100% height=50% alt="Image">

						</div>

						<div class="tm-slideshow-description tm-slideshow-description-left tm-bg-highlight">
								<h2 class="">Cantón la Mana</h2>
								<p>La Maná, El Encanto de Cotopaxi, se localiza al finalizar la  cordillera Occidental de los Andes. Su clima es privilegiado por estar al inicio de la región costera y posee una hidrografía y biodiversidad que enamoran, con uno de sus principales atractivos, las 7 cascadas del Zapanal.

                   Este cantón es ahora un nuevo destino turístico que  toma más fuerza después de la pandemia.
                   Luego de este hecho de impacto mundial, el turismo está entre una las prioridades de la actual
									  administración municipal del alcalde Hipólito Carrera, quien por medio de la Empresa Pública de
										Turismo y Comunicación Social (Emturcom) ejecuta acciones para aprovechar al máximo las cascadas,
										 complejos y fincas agroecológicas, estas últimas localizadas en las parroquias Guasaganda y Pucayacu.</p>
								<!-- <a href="#" class="text-uppercase tm-btn tm-btn-white tm-btn-white-highlight">Continue Reading</a> -->
						</div>

				</section>
				<!-- <section class="tm-slideshow-section">
						<div class="tm-slideshow">
								<img src="<?php echo base_url(); ?>assets/img/tm-img-03.jpg" alt="Image">
								<img src="<?php echo base_url(); ?>assets/img/tm-img-02.jpg" alt="Image">
								<img src="<?php echo base_url(); ?>assets/img/tm-img-01.jpg" alt="Image">
						</div>
						<div class="tm-slideshow-description tm-bg-primary">
								<h2 class="">America's most famous places</h2>
								<p>Donec nec laoreet diam, at vehicula ante. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse nec dapibus nunc, quis viverra metus. Morbi eget diam gravida, euismod magna vel, tempor urna.</p>
								<a href="#" class="text-uppercase tm-btn tm-btn-white tm-btn-white-primary">Continue Reading</a>
						</div>
				</section> -->
		</div>
		<!-- <div class="tm-container-outer" id="tm-section-3">
				<ul class="nav nav-pills tm-tabs-links">
						<li class="tm-tab-link-li">
								<a href="#1a" data-toggle="tab" class="tm-tab-link">
										<img src="img/north-america.png" alt="Image" class="img-fluid">
										North America
								</a>
						</li>
						<li class="tm-tab-link-li">
								<a href="#2a" data-toggle="tab" class="tm-tab-link">
										<img src="<?php echo base_url(); ?>assets/img/south-america.png" alt="Image" class="img-fluid">
										South America
								</a>
						</li>
						<li class="tm-tab-link-li">
								<a href="#3a" data-toggle="tab" class="tm-tab-link">
										<img src="<?php echo base_url(); ?>assets/img/europe.png" alt="Image" class="img-fluid">
										Europe
								</a>
						</li>
						<li class="tm-tab-link-li">
								<a href="#4a" data-toggle="tab" class="tm-tab-link active"><!-- Current Active Tab -->
										<!-- <img src="img/asia.png" alt="Image" class="img-fluid">
										Asia
								</a>
						</li>
						<li class="tm-tab-link-li">
								<a href="#5a" data-toggle="tab" class="tm-tab-link">
										<img src="<?php echo base_url(); ?>assets/img/africa.png" alt="Image" class="img-fluid">
										Africa
								</a>
						</li>
						<li class="tm-tab-link-li">
								<a href="#6a" data-toggle="tab" class="tm-tab-link">
										<img src="<?php echo base_url(); ?>assets/img/australia.png" alt="Image" class="img-fluid">
										Australia
								</a>
						</li>
						<li class="tm-tab-link-li">
								<a href="#7a" data-toggle="tab" class="tm-tab-link">
										<img src="<?php echo base_url(); ?>assets/img/antartica.png" alt="Image" class="img-fluid">
										Antartica
								</a>
						</li>
				</ul>
				<div class="tab-content clearfix">  -->

						<!-- Tab 1 -->
						<!-- <div class="tab-pane fade" id="1a">
								<div class="tm-recommended-place-wrap">
										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-06.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">North Garden Resort</h3>
														<p class="tm-text-highlight">One North</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$110</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-07.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Felis nec dignissim</h3>
														<p class="tm-text-highlight">Two North</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<div id="preload-hover-img"></div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$120</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-05.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Sed fermentum justo</h3>
														<p class="tm-text-highlight">Three North</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$130</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-04.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Maecenas ultricies neque</h3>
														<p class="tm-text-highlight">Four North</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$140</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>
								</div>

								<!-- <a href="#" class="text-uppercase btn-primary tm-btn mx-auto tm-d-table">Show More Places</a> -->
						</div> <!-- tab-pane -->

						<!-- Tab 2 -->
						<!-- <div class="tab-pane fade" id="2a">

								<div class="tm-recommended-place-wrap">
										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-05.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">South Resort Hotel</h3>
														<p class="tm-text-highlight">South One</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$220</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-04.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Aenean ac ante nec diam</h3>
														<p class="tm-text-highlight">South Second</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$230</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-07.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Suspendisse nec dapibus</h3>
														<p class="tm-text-highlight">South Third</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$240</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-06.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Aliquam viverra mi at nisl</h3>
														<p class="tm-text-highlight">South Fourth</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$250</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>
								</div>

								<!-- <a href="#" class="text-uppercase btn-primary tm-btn mx-auto tm-d-table">Show More Places</a> -->
						</div> <!-- tab-pane -->

						<!-- Tab 3 -->
						<!-- <div class="tab-pane fade" id="3a">

								<div class="tm-recommended-place-wrap">
										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-04.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Europe Hotel</h3>
														<p class="tm-text-highlight">Venecia, Italy</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$330</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-05.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">In viverra enim turpis</h3>
														<p class="tm-text-highlight">Paris, France</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$340</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-06.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Volutpat pellentesque</h3>
														<p class="tm-text-highlight">Barcelona, Spain</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$350</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-07.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Grand Resort Pasha</h3>
														<p class="tm-text-highlight">Istanbul, Turkey</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$360</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>
								</div>

								<!-- <a href="#" class="text-uppercase btn-primary tm-btn mx-auto tm-d-table">Show More Places</a> -->
						</div> <!-- tab-pane -->

						<!-- Tab 4 -->
						<!-- <div class="tab-pane fade show active" id="4a"> -->
						<!-- Current Active Tab WITH "show active" classes in DIV tag -->
								<!-- <div class="tm-recommended-place-wrap">
										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-06.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Asia Resort Hotel</h3>
														<p class="tm-text-highlight">Singapore</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$440</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-07.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Nullam eget est a nisl</h3>
														<p class="tm-text-highlight">Yangon, Myanmar</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<div id="preload-hover-img"></div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$450</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-05.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Proin interdum ullamcorper</h3>
														<p class="tm-text-highlight">Bangkok, Thailand</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$460</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-04.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Lorem ipsum dolor sit</h3>
														<p class="tm-text-highlight">Vientiane, Laos</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$470</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>
								</div>

								<a href="#" class="text-uppercase btn-primary tm-btn mx-auto tm-d-table">Regresar al inico</a>
						</div> <!-- tab-pane -->

						<!-- Tab 5 -->
						<!-- <div class="tab-pane fade" id="5a">

								<div class="tm-recommended-place-wrap">
										<div class="tm-recommended-place">
												<img src="<?php echo base_url(); ?>assets/img/tm-img-05.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Africa Resort Hotel</h3>
														<p class="tm-text-highlight">First City</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$550</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-04.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Aenean ac magna diam</h3>
														<p class="tm-text-highlight">Second City</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$560</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-07.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Beach Barbecue Sunset</h3>
														<p class="tm-text-highlight">Third City</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$570</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-06.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Grand Resort Pasha</h3>
														<p class="tm-text-highlight">Fourth City</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$580</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>
								</div>

								<a href="#" class="text-uppercase btn-primary tm-btn mx-auto tm-d-table">Show More Places</a>
						</div> <!-- tab-pane -->

						<!-- Tab 6 -->
						<!-- <div class="tab-pane fade" id="6a">

								<div class="tm-recommended-place-wrap">
										<div class="tm-recommended-place">
												<img src="img/tm-img-04.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Hotel Australia</h3>
														<p class="tm-text-highlight">City One</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$660</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-05.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Proin interdum ullamcorper</h3>
														<p class="tm-text-highlight">City Two</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$650</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-06.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Beach Barbecue Sunset</h3>
														<p class="tm-text-highlight">City Three</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$640</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-07.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Grand Resort Pasha</h3>
														<p class="tm-text-highlight">City Four</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$630</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>
								</div>

								<a href="#" class="text-uppercase btn-primary tm-btn mx-auto tm-d-table">Show More Places</a>
						</div> <!-- tab-pane -->

						<!-- Tab 7 -->
						<!-- <div class="tab-pane fade" id="7a">

								<div class="tm-recommended-place-wrap">
										<div class="tm-recommended-place">
												<img src="img/tm-img-04.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Antartica Resort</h3>
														<p class="tm-text-highlight">Ant City One</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$770</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-05.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Pulvinar Semper</h3>
														<p class="tm-text-highlight">Ant City Two</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$230</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-06.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Cras vel sapien</h3>
														<p class="tm-text-highlight">Ant City Three</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$140</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>

										<div class="tm-recommended-place">
												<img src="img/tm-img-07.jpg" alt="Image" class="img-fluid tm-recommended-img">
												<div class="tm-recommended-description-box">
														<h3 class="tm-recommended-title">Nullam eget est</h3>
														<p class="tm-text-highlight">Ant City Four</p>
														<p class="tm-text-gray">Sed egestas, odio nec bibendum mattis, quam odio hendrerit risus, eu varius eros lacus sit amet lectus. Donec blandit luctus dictum...</p>
												</div>
												<a href="#" class="tm-recommended-price-box">
														<p class="tm-recommended-price">$190</p>
														<p class="tm-recommended-price-link">Continue Reading</p>
												</a>
										</div>
								</div>

<<<<<<< HEAD
								<a href="#" class="text-uppercase btn-primary tm-btn mx-auto tm-d-table">Show More Places</a>
						</div>
				</div>
		</div> -->

		<!-- <div class="tm-container-outer tm-position-relative" id="tm-section-4">
				<div id="google-map"></div>
				<form action="index.html" method="post" class="tm-contact-form">
						<div class="form-group tm-name-container">
								<input type="text" id="contact_name" name="contact_name" class="form-control" placeholder="Name"  required/>
						</div>
						<div class="form-group tm-email-container">
								<input type="email" id="contact_email" name="contact_email" class="form-control" placeholder="Email"  required/>
						</div>
						<div class="form-group">
								<input type="text" id="contact_subject" name="contact_subject" class="form-control" placeholder="Subject"  required/>
						</div>
						<div class="form-group">
								<textarea id="contact_message" name="contact_message" class="form-control" rows="9" placeholder="Message" required></textarea>
						</div>
						<button type="submit" class="btn btn-primary tm-btn-primary tm-btn-send text-uppercase">Send Message Now</button>
				</form>
		</div> <!-- .tm-container-outer -->
								<!-- <a href="#" class="text-uppercase btn-primary tm-btn mx-auto tm-d-table">Show More Place</a> -->
						</div> <!-- tab-pane -->
				</div>
		</div>
