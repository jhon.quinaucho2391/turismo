<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtaUfCa9wOGq3J8mJCDV9QyP9bcBezZbY&libraries=places&callback=initMap" >
      </script>
<div class="tm-container-outer tm-bg-primary" style="padding: 7px; color: white;">
  <div class="row">
    <div class="col-md-8" style="">
      <h2 class=""> <b> EDITAR LUGAR </b> </h2>
    </div>
    <!-- <div class="col-md-4 text-right">
      <h2 class=""> <b> <?php echo $rutaEditar->lugar_ruta; ?> </b> </h2>
    </div> -->


  </div>
</div>

<div class="tm-container-outer" style="padding: 15px; color: white;">
  <div class="row">
    <div class="col-md-12" style="background:white; color: black;">
      <br>
      <?php if ($LugarEditar): ?>
        <form class="" id="frm_editar_ruta" class="" enctype="multipart/form-data" action="<?php echo site_url('lugares/procesarActualizacion'); ?>" method="post">
           <input type="hidden" id="fk_id_ruta" name="fk_id_ruta" value="<?php echo $LugarEditar->fk_id_ruta; ?>" class="form-control" placeholder="Ingrese el nombre de la fase" required>
          <input type="hidden" name="id_lugar" value="<?php echo $LugarEditar->id_lugar; ?>">
          <div class="row">
            <div class="col-md-4">
              <div class="col-md-12">
                    <label for="">NOMBRE </label>
                    <input type="text" id="nombre_lugar" name="nombre_lugar" value="<?php echo $LugarEditar->nombre_lugar; ?>" class="form-control" placeholder="Ingrese el nombre de la fase" required>
                <br>
              </div>
            </div>
            <div class="col-md-4">
              <div class="col-md-12">
                    <label for="">ALTURA </label>
                    <input type="text" id="altura_lugar" name="altura_lugar" value="<?php echo $LugarEditar->altura_lugar; ?>" class="form-control" placeholder="Ingrese el nombre de la fase" required>
                <br>
              </div>
            </div>
            <div class="col-md-4">
              <div class="col-md-12">
                    <label for="">LATITUD</label>
                    <input type="text" id="latitud_lugar" name="latitud_lugar" value="<?php echo $LugarEditar->latitud_lugar; ?>" class="form-control" placeholder="Ingrese el nombre de la fase" required>
                <br>
              </div>
            </div>
            <div class="col-md-4">
              <div class="col-md-12">
                    <label for="">LONGITUD</label>
                    <input type="text" id="longitud_lugar" name="longitud_lugar" value="<?php echo $LugarEditar->longitud_lugar; ?>" class="form-control" placeholder="Ingrese el nombre de la fase" required>
                <br>
              </div>
            </div>
          </div>

    <div class="col-md-10 ">
      <label for="">UBICACIONES</label>
      <div id="mapa_cliente"
      style="height:300px; width:100%;
      border:2px solid black;">
      </div>
  </div>
    <div class="row">
      <div class="col-md-4">
      </div>
    </div>

    <script type="text/javascript">
    function initMap(){
 // alert("Correcto")
     var latitud_longitud=new google.maps.LatLng(<?php echo $LugarEditar->latitud_lugar; ?>, <?php echo $LugarEditar->longitud_lugar; ?>);
     var mapa=new google.maps.Map(
         document.getElementById('mapa_cliente'),
         {
           center:latitud_longitud,
           zoom:16,
           mapTypeId:google.maps.MapTypeId.ROADMAP
         }
       );
       var marcador=new google.maps.Marker({
         position:latitud_longitud,
         map:mapa,
         title:"seleccione la direccion",
         draggable:true
       });

       google.maps.event.addListener(
         marcador,
         'dragend',
         function (event){
           var latitud=this.getPosition().lat();
           var longitud=this.getPosition().lng();
           document.getElementById("latitud_lugar").value=latitud;
           document.getElementById("longitud_lugar").value=longitud;
           // alert("LATITUD:"+latitud);
           // alert("LONGITUD"+longitud);
         }
       );
      }

    </script>
    <div class="modal-footer">
      <a href="<?php echo site_url('lugares/index'); ?>/<?php echo $LugarEditar->fk_id_ruta; ?>"class="btn btn-danger"> <i class="fa fa-arrow-left" aria-hidden="true"></i></a>

      <button type="submit" class="btn btn-primary">Actualizar</button>
    </div>
    </div>
        </form>
      <?php endif; ?>
    </div>
  </div>
</div>
