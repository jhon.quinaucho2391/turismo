
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtaUfCa9wOGq3J8mJCDV9QyP9bcBezZbY&libraries=places&callback=initMap" >
      </script>
<div class="tm-container-outer tm-bg-primary" style="padding: 7px; color: white;">
  <div class="row">

    <div class="col-md-8" style="">


      <h3> <b> <?php echo $rutas->lugar_ruta ?> </b> </h3>
    </div>

    <div class="col-md-4 text-right">
      <a href="#" class="text-uppercase tm-btn tm-btn-white tm-btn-white-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i> </a>
      <a href="<?php echo site_url('rutas/index'); ?>"class="text-uppercase tm-btn tm-btn-white tm-btn-white-primary"> <i class="fa fa-arrow-left" aria-hidden="true"></i></a>
    </div>

  </div>

</div>
<!-- <a href="<?php echo site_url('rutas/index'); ?>"class="btn btn-primary">regresar</a> -->
<div class="tm-container-outer" style="padding: 15px; color: white;">

  <div class="row">
    <div class="col-md-12" style="background:white; color: black;">
      <br>

      <?php if ($listadoLugares): ?>
        <table id="tbl-lugares" class="table table-striped table-bordered table-hover" >
          <thead>

            <tr>
              <!-- <th class="text-center">ID</th> -->
              <th class="text-center">NOMBRE</th>
              <th class="text-center">ALTURA</th>
                <th class="text-center">LATITUD</th>
                  <th class="text-center">LONGITUD</th>
              <th class="text-center">ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoLugares->result() as $lugarTemporal): ?>
              <tr>
                <!-- <td class="text-center"><?php echo $rutaTemporal->id_lugar; ?></td> -->
                <td class="text-center"><?php echo $lugarTemporal->nombre_lugar; ?></td>
                  <td class="text-center"><?php echo $lugarTemporal->altura_lugar; ?></td>
                <td class="text-center"><?php echo $lugarTemporal->latitud_lugar; ?></td>
                <td class="text-center"><?php echo $lugarTemporal->longitud_lugar; ?></td>

                <td class="text-center">
                 <a href="<?php echo site_url('lugares/actualizar'); ?>/<?php echo $lugarTemporal->id_lugar; ?>/<?php echo $lugarTemporal->fk_id_ruta; ?>" class=""> <i class="fa fa-pencil orange-icon" ></i></a>
                <style media="screen">
                  .orange-icon {
                   color: #ecab0f ;
                   font-size: 18px;
                    }
                  </style>
                 &nbsp

                  <a href="<?php echo site_url('lugares/borrar'); ?>/<?php echo $lugarTemporal->id_lugar; ?>/<?php echo $lugarTemporal->fk_id_ruta; ?>"class=""> <i class="fa fa-trash red-icon"></i></a>
                  <style media="screen">
                    .red-icon {
                     color: red;
                     font-size: 18px;
                      }
                    </style>
              <!-- <a href="<?php echo site_url('lugares/index'); ?>"class="btn btn-primary">GESTIONAR</a> -->
             </tr>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php else: ?>
        <h3><b>LUGARES NO REGISTRADAS</b></h3>
      <?php endif; ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#tbl-lugares").DataTable({
    language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json'
          }
    });
</script>
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <br><br><br><br><br>
    <div class="modal-content">

      <!-- Encabezado del modal -->
      <div class="modal-header">
        <h5 class="modal-title">LUGARES</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Contenido del modal -->
      <div class="modal-body">
        <form class="" id="frm_nuevo_tipo" class="" enctype="multipart/form-data" action="<?php echo site_url('lugares/guardarLugar'); ?>" method="post">
          <input type="hidden" id="fk_id_ruta" name="fk_id_ruta" value="<?php echo $rutas->id_ruta ?>" class="form-control" placeholder="Ingrese el nombre de la fase" required>
          <div class="row">
            <div class="col-md-4">
              <div class="col-md-12">
                    <label for="">NOMBRE </label>
                    <input type="text" id="nombre_lugar" name="nombre_lugar" value="" class="form-control" placeholder="Ingrese el nombre de la fase" required>
                <br>
              </div>
            </div>
            <div class="col-md-4">
              <div class="col-md-12">
                    <label for="">ALTURA </label>
                    <input type="text" id="altura_lugar" name="altura_lugar" value="" class="form-control" placeholder="Ingrese el nombre de la fase" required>
                <br>
              </div>
            </div>
            <div class="col-md-4">
              <div class="col-md-12">
                    <label for="">LATITUD</label>
                    <input type="text" id="latitud_lugar" name="latitud_lugar" value="" class="form-control" placeholder="Ingrese el nombre de la fase" required readonly>
                <br>
              </div>
            </div>
            <div class="col-md-4">
              <div class="col-md-12">
                    <label for="">LONGITUD</label>
                    <input type="text" id="longitud_lugar" name="longitud_lugar" value="" class="form-control" placeholder="Ingrese el nombre de la fase" required readonly>
                <br>
              </div>
            </div>
          </div>

    <div class="col-md-10 ">
      <label for="">UBICACIONES</label>
      <div id="mapa_cliente"
      style="height:300px; width:100%;
      border:2px solid black;">
      </div>
  </div>
    <div class="row">
      <div class="col-md-4">
      </div>
    </div>

    <script type="text/javascript">
    function initMap(){
 // alert("Correcto")
     var latitud_longitud=new google.maps.LatLng(-0.9378050282063829, -79.23100346937962);
     var mapa=new google.maps.Map(
         document.getElementById('mapa_cliente'),
         {
           center:latitud_longitud,
           zoom:16,
           mapTypeId:google.maps.MapTypeId.ROADMAP
         }
       );
       var marcador=new google.maps.Marker({
         position:latitud_longitud,
         map:mapa,
         title:"seleccione la direccion",
         draggable:true
       });

       google.maps.event.addListener(
         marcador,
         'dragend',
         function (event){
           var latitud=this.getPosition().lat();
           var longitud=this.getPosition().lng();
           document.getElementById("latitud_lugar").value=latitud;
           document.getElementById("longitud_lugar").value=longitud;
           // alert("LATITUD:"+latitud);
           // alert("LONGITUD"+longitud);
         }
       );
      }

    </script>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      <button type="submit" class="btn btn-primary">Guardar cambios</button>
    </div>
    </div>
  </form>


      <!-- Pie del modal -->


    </div>
  </div>
</div>


















<!-- Script de validacion -->
<script type="text/javascript">

$("#frm_nuevo_tipo").validate({
  rules:{
    nombre_fas_ej:{
      required:true,
      minlength:3
    }
  },

  messages:{
    nombre_fas_ej:{
      required:"Ingrese el nombre de la acción",
      minlength:"Nombre Incorrecto"
    }
  }
});

</script>
