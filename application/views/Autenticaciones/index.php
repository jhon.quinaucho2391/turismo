<div class="tm-container-outer tm-bg-primary" style="padding: 7px; color: white;">
  <div class="row">

    <div class="col-md-8" style="">


      <h3> <b> Registro de Logs </b> </h3>
    </div>

    <div class="col-md-4 text-right">
</div>

  </div>

</div>
<div class="tm-container-outer" style="padding: 15px; color: white;">
  <div class="row">
    <div class="col-md-12" style="background:white; color: black;">
      <br>
      <?php if ($listadoslogs): ?>
      <table id="tbl-funciones" class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-center">ID</td>
            <td class="text-center">detalle</td>
            <td class="text-center">fecha</td>
            <td class="text-center">usuario</td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoslogs->result() as $boletosTemporal): ?>
            <tr>
              <td class="text-center"><?php echo $boletosTemporal->id_log ?></td>
              <td class="text-center"><?php echo $boletosTemporal->detalle_log ?></td>
              <td class="text-center"><?php echo $boletosTemporal->fecha_log ?></td>
              <td class="text-center"><?php echo $boletosTemporal->nombre_usu," ",$boletosTemporal->primer_apellido_usu?></td>
            </tr>

          <?php endforeach; ?>
        </tbody>
      </table>
      <script type="text/javascript">
        $("#tbl-funciones").DataTable({
          language: {
                  url: 'https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json'
                }
          });

      </script>
    <?php else: ?>
    <h3>
      <b>No existe Funciones</b>
    </h3>
    <?php endif; ?>
    </div>
  </div>
</div>
