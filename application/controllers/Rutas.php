<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rutas extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
    $this->load->model("ruta");
		if ($this->session->userdata("conectad0"))
    {
     //SI A INICIADO session
     } else {
     	redirect("/");
    }
	}
  public function index(){
		$data["listadoRutas"]=$this->ruta->obtenerTodos();
		$id = 1; // Aquí debes proporcionar el valor de ID adecuado
	 	// Verifica si el ID existe y obtiene los datos de la ruta para la edición
	  if (!empty($id)) {
				$data['rutaEditar'] = $this->ruta->obtenerPorId($id);
		} else {
				 $data['rutaEditar'] = null; // Si el ID no está disponible, establece el valor a null o deja el arreglo vacío
		}
    $this->load->view("header.php");
		$this->load->view("rutas/index.php",$data);
		$this->load->view("footer.php");
  }
	// Funcion para guardar ruta
  public function guardarRuta(){
    $datosNuevoRutas=array(
      'lugar_ruta'=>$this->input->post('lugar_ruta'),
      'codigo_ruta'=>$this->input->post('codigo_ruta'),
      'duracion_ruta'=>$this->input->post('duracion_ruta'),
      'grupo_ruta'=>$this->input->post('grupo_ruta'),
      'dificultad_ruta'=>$this->input->post('dificultad_ruta'),
      'audiencia_ruta'=>$this->input->post('audiencia_ruta'),
      'atractivos_ruta'=>$this->input->post('atractivos_ruta')
    );
    print_r($datosNuevoRutas);
    if ($this->ruta->insertar($datosNuevoRutas)) {
      $this->session->set_flashdata('confirmacion','Accion insertado exitosamente');
    }else {
      $this->session->set_flashdata('error','Error al insertar, verifique e intente de nuevo');
    }
    redirect('rutas/index');
  }
  //Funcion para eliminar fase
  public function borrar($id_ruta){
    if ($this->ruta->eliminarPorId($id_ruta)) {
      $this->session->set_flashdata('confirmacion','Eliminado exitosamente');
    } else {
      $this->session->set_flashdata('error','Error al ELIMINAR verifique e intente de nuevo');
    }
    redirect('rutas/index');
  }
  // Actualizar datos
  public function actualizar($id){
    $data['rutaEditar']=$this->ruta->obtenerPorId($id);
    $this->load->view('header.php');
    $this->load->view('rutas/actualizar',$data);
    $this->load->view('footer.php');
  }

  //Procesar actualizacion
	public function procesarActualizacion(){
		$datosRutasEditado=array(
			'lugar_ruta'=>$this->input->post('lugar_ruta'),
			'codigo_ruta'=>$this->input->post('codigo_ruta'),
			'duracion_ruta'=>$this->input->post('duracion_ruta'),
			'grupo_ruta'=>$this->input->post('grupo_ruta'),
			'dificultad_ruta'=>$this->input->post('dificultad_ruta'),
			'audiencia_ruta'=>$this->input->post('audiencia_ruta'),
			'atractivos_ruta'=>$this->input->post('atractivos_ruta')
		);

		$id=$this->input->post('id_ruta');
		if ($this->ruta->actualizar($datosRutasEditado,$id)) {
			$this->session->set_flashdata('confirmacion','Editado exitosamente');
		}else {
			$this->session->set_flashdata('error','Error al Actualizar verifique e intente de nuevo');
		}
		redirect('rutas/index');
	}
}//Cierre de la Clase
