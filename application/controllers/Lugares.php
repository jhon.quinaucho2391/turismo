<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lugares extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
   $this->load->model("lugar");
	 $this->load->model("ruta");
		 if ($this->session->userdata("conectad0"))
		 {
			//SI A INICIADO session
			} else {
			 redirect("/");
		 }
	}
  public function index($id_ruta){
		// $data["listadoLugares"]=$this->lugar->obtenerTodos();
		$data["listadoLugares"]=$this->lugar->obtenerPorId($id_ruta);
		$data["rutas"]=$this->ruta->obtenerPorId($id_ruta);
    $this->load->view("header.php");
		$this->load->view("lugares/index.php",$data,$id_ruta);
		$this->load->view("footer.php");
  }
  public function guardarLugar(){
		$fk_id_ruta=$this->input->post('fk_id_ruta');
    $datosNuevoLugar=array(
      'nombre_lugar'=>$this->input->post('nombre_lugar'),
      'altura_lugar'=>$this->input->post('altura_lugar'),
      'latitud_lugar'=>$this->input->post('latitud_lugar'),
      'longitud_lugar'=>$this->input->post('longitud_lugar'),
			'fk_id_ruta'=>$fk_id_ruta

    );
    //Guardar fase si hay o no existe es un if de guardar fase
    print_r($datosNuevoLugar);
    if ($this->lugar->insertar($datosNuevoLugar)) {
      $this->session->set_flashdata('confirmacion','Accion insertado exitosamente');
    }else {
      $this->session->set_flashdata('error','Error al insertar, verifique e intente de nuevo');
    }
    redirect('lugares/index/'.$fk_id_ruta);
  }
  //Funcion para eliminar fase
  public function borrar($id_lugar,$fk_id_ruta){
    if ($this->lugar->eliminarPorId($id_lugar)) {
      $this->session->set_flashdata('confirmacion','Eliminado exitosamente');
    } else {
      $this->session->set_flashdata('error','Error al ELIMINAR verifique e intente de nuevo');
    }
    redirect('lugares/index/'.$fk_id_ruta);
  }
  // Actualizar datos
  public function actualizar($id){
    $data['LugarEditar']=$this->lugar->obtenerPorIdLugar($id);
    $this->load->view('header.php');
    $this->load->view('lugares/actualizar',$data);
    $this->load->view('footer.php');
  }

  //Procesar actualizacion
  public function procesarActualizacion(){
		$fk_id_ruta=$this->input->post('fk_id_ruta');
    $lugarEditado=array(
			'nombre_lugar'=>$this->input->post('nombre_lugar'),
      'altura_lugar'=>$this->input->post('altura_lugar'),
      'latitud_lugar'=>$this->input->post('latitud_lugar'),
      'longitud_lugar'=>$this->input->post('longitud_lugar'),
			'fk_id_ruta'=>$this->input->post('fk_id_ruta')
    );
    $id=$this->input->post('id_lugar');
    if ($this->lugar->actualizar($lugarEditado,$id)) {
      $this->session->set_flashdata('confirmacion','Editado exitosamente');
    }else {
      $this->session->set_flashdata('error','Error al Actualizar verifique e intente de nuevo');
    }
		// echo $fk_id_ruta;
    redirect('lugares/index/'.$fk_id_ruta);
  }
}//Cierre de la Clase
