<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
    $this->load->model("usuario");
		if ($this->session->userdata("conectad0"))
    {
     //SI A INICIADO session
     } else {
     	redirect("/");
    }
	}
  public function index(){
    $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
    $this->load->view("header.php");
		$this->load->view("usuarios/index.php",$data);
		$this->load->view("footer.php");
  }
  public function guardarUsuarios(){
  $datosUsuario=array(
    "nombre_usu"=>$this->input->post('nombre_usu'),
    "primer_apellido_usu"=>$this->input->post('primer_apellido_usu'),
    "segundo_apellido_usu"=>$this->input->post('segundo_apellido_usu'),
    "email_usu"=>$this->input->post('email_usu'),
		"rol_usu"=>$this->input->post('rol_usu'),
    "password_usu"=>$this->input->post('password_usu')

  );
  print_r($datosUsuario);
  if ($this->usuario->insertar($datosUsuario)) {
    redirect('usuarios/index');
  } else {
    echo "error";
  }

}
public function actualizar($id){
	$data["usuariosEditar"]=$this->usuario->obtenerPorId($id);
	$this->load->view('header.php');
	$this->load->view('usuarios/actualizar',$data);
	$this->load->view('footer.php');
}

/*public function procesoActualizar(){
	$datosUsuariosEditados=array(
    "nombre_usu"=>$this->input->post('nombre_usu'),
    "primer_apellido_usu"=>$this->input->post('primer_apellido_usu'),
    "segundo_apellido_usu"=>$this->input->post('segundo_apellido_usu'),
    "email_usu"=>$this->input->post('email_usu'),
		"rol_usu"=>$this->input->post('rol_usu'),
    "password_usu"=>$this->input->post('password_usu')
	);
	$id=$this->input->post("id_usu");
	if ($this->usuario->actualizar($id,$datosUsuariosEditados)) {
		$this->session->set_flashdata('confirmacion','Editado exitosamente');
	} else {
		$this->session->set_flashdata('error','Error al Actualizar verifique e intente de nuevo');
	}
	redirect("usuarios/index");
}*/

public function procesoActualizacion(){
	$datosUsuariosEditados=array(
		"nombre_usu"=>$this->input->post('nombre_usu'),
    "primer_apellido_usu"=>$this->input->post('primer_apellido_usu'),
    "segundo_apellido_usu"=>$this->input->post('segundo_apellido_usu'),
    "email_usu"=>$this->input->post('email_usu'),
		"rol_usu"=>$this->input->post('rol_usu'),
    "password_usu"=>$this->input->post('password_usu')
	);

	$id=$this->input->post('id_usu');
	if ($this->usuario->actualizar($datosUsuariosEditados,$id)) {
		$this->session->set_flashdata('confirmacion','Editado exitosamente');
	}else {
		$this->session->set_flashdata('error','Error al Actualizar verifique e intente de nuevo');
	}
	redirect('usuarios/index');
}

public function borrar($id){
	if ($this->usuario->eliminarPorId($id)) {
		$this->session->set_flashdata('confirmacion','Usaurio ELIMINADA exitosamente');
	} else {
		echo "error al eliminar";
	}
	redirect('usuarios/index');


}




  }
