<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	//definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
    $this->load->model("usuario");
		$this->load->model("autenticacion");
	}
  public function index(){
    $this->load->view("header.php");
		$this->load->view("welcome_message.php");
		$this->load->view("footer.php");
  }

  public function validarUsuario(){
    $email=$this->input->post("email_usu");
    $password=$this->input->post("password_usu");
    $usuarioEncontrado=$this->usuario->obtenerEmailPassword($email,$password);
    if($usuarioEncontrado){
      //Cuando el email y password estan BIEN
      //Se debe crear la sesion
      $this->session->set_userdata('conectad0',$usuarioEncontrado);
			$dataLogs=array(
			"fk_id_usu"=>$usuarioEncontrado->id_usu,
			"detalle_log"=>"INGRESAR"
			);
			$this->autenticacion->insertar($dataLogs);
      $this->session->set_flashdata("bienvenida","Saludos, bienvenido al sistema");
      redirect("rutas/index");
    }else{
      //Cuando el email y/o password estan MAL
      $this->session->set_flashdata("error","Email o contraseña incorrectos");
      redirect("welcome/index");
    }

  }

  public function cerrarSesion(){
      $this->session->sess_destroy();//Matando la sesiones
      redirect("welcome/index");
    }
}//Cierre de la Clase
