<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autenticaciones extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("autenticacion");
		if ($this->session->userdata("conectad0"))
    {
     //SI A INICIADO session
     } else {
     	redirect("/");
    }
	}
  public function index(){
    $data["listadoslogs"]=$this->autenticacion->obtenerTodas();
    $this->load->view("header.php");
    $this->load->view("autenticaciones/index.php",$data);
    $this->load->view("footer.php");
  }




}
