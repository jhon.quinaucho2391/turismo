<?php
    class Usuario extends CI_Model{
        function __construct()
        {
            parent::__construct();
        }
        //definimos funcion para consultar email y contraseñas
        public function obtenerEmailPassword($email,$password){
            $this->db->where("email_usu",$email);
            $this->db->where("password_usu",$password);
            $usuarios=$this->db->get("usuario");
            if ($usuarios->num_rows()>0) {
                return $usuarios->row();
            } else {
                return false;//cuando no exista datos
            }
        }
        public function insertar($datos)
        {
          return $this->db->insert('usuario',$datos);
        }

        public function obtenerTodos(){
          $usuarios=$this->db->get('usuario');
          if ($usuarios->num_rows()>0) {
            return $usuarios;
          } else {
            return false;
          }

        }

        public function eliminarPorId($id){
          $this->db->where('id_usu',$id);
          return $this->db->delete('usuario');
        }

        public function obtenerPorId($id){
          $this->db->where('id_usu',$id);
          $usuarios=$this->db->get('usuario');
          if ($usuarios->num_rows()>0) {
            return $usuarios->row();
          } else {
            return false;
          }

        }

        public function actualizar($datos,$id){
          $this->db->where('id_usu',$id);
          return $this->db->update('usuario',$datos);
        }

    }
