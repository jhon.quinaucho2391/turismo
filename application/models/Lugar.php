<?php
  class Lugar extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }
    public function insertar($datos)
    {
      return $this->db->insert('lugar',$datos);
    }
    //Funsion de consulta de todos los estudiantes
    public function obtenerTodos(){
      $lugares=$this->db->get('lugar');
      if ($lugares->num_rows()>0) {
        return $lugares;
      } else {
        return false; //cuando no hay datos
      }

    }
    //Funcion para eliminar un estudiante se recbe el id_est
    public function eliminarPorId($id_lugar){
      $this->db->where('id_lugar',$id_lugar);
      return $this->db->delete('lugar');
    }

    // public function obtenerPorId($id_lugar){
    //   $this->db->where('id_lugar',$id_lugar);
    //   $lugares=$this->db->get('lugar');
    //   if ($lugares->num_rows()>0) {
    //     return $lugares->row(); //porque solo existe uno
    //   } else {
    //     return false;
    //   }
    //
    // }

    public function obtenerPorId($id_lugar){
      $this->db->join("rutas","rutas.id_ruta=lugar.fk_id_ruta");
      $this->db->where('fk_id_ruta',$id_lugar);
      // $this->db->where('id_jug_ej',$id);
      $lugares=$this->db->get('lugar');
      if ($lugares->num_rows()>0) {
        return $lugares; //porque solo existe uno
      } else {
        return false;
      }

    }

    public function obtenerPorIdLugar($id_lugar){
      $this->db->where('id_lugar',$id_lugar);
      $lugares=$this->db->get('lugar');
      if ($lugares->num_rows()>0) {
        return $lugares->row(); //porque solo existe uno
      } else {
        return false;
      }

    }

    //Proceso de actualizacion de estudiante
    public function actualizar($datos,$id_lugar){
      $this->db->where('id_lugar',$id_lugar);
      return $this->db->update('lugar',$datos);
    }
  } //Cierre de la clase (No borrar)
