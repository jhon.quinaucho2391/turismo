<?php
  class Ruta extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }
    public function insertar($datos)
    {
      return $this->db->insert('rutas',$datos);
    }
    //Funsion de consulta de todos los estudiantes
    public function obtenerTodos(){
      $rutas=$this->db->get('rutas');
      if ($rutas->num_rows()>0) {
        return $rutas;
      } else {
        return false; //cuando no hay datos
      }

    }
    //Funcion para eliminar un estudiante se recbe el id_est
    public function eliminarPorId($id){
      $this->db->where('id_ruta',$id);
      return $this->db->delete('rutas');
    }

    public function obtenerPorId($id){
      $this->db->where('id_ruta',$id);
      $rutas=$this->db->get('rutas');
      if ($rutas->num_rows()>0) {
        return $rutas->row(); //porque solo existe uno
      } else {
        return false;
      }

    }

    // Proceso de actualizacion de estudiante
    public function actualizar($datos,$id){
      $this->db->where('id_ruta',$id);
      return $this->db->update('rutas',$datos);
    }

  } //Cierre de la clase (No borrar)
